FROM python:3.9-slim
RUN pip install --no-cache notebook jupyterlab
ENV HOME=/tmp

# create user with a home directory
ARG NB_USER
ARG NB_UID
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}


RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}
WORKDIR ${HOME}

RUN  HDF5_MPI="ON" HDF5_DIR="/usr/lib/x86_64-linux-gnu/hdf5/mpich/" CC=mpicc pip3 install --no-cache-dir --no-binary=h5py h5py meshio
RUN pip3 install setuptools jupyterhub nbconvert --upgrade
RUN pip3 install --no-cache-dir matplotlib itkwidgets ipywidgets ipyvtklink pyvista seaborn pandas
RUN jupyter labextension install jupyter-matplotlib jupyterlab-datawidgets itkwidgets && \
    rm -rf /usr/local/share/.cache/*
